package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

/**
 * Класс с тестами для {@link MyBranching}
 */
public class MyBranchingTest {
    /**
     * Проверяем успешный сценарий выполнения maxInt
     */
    @Test
    public void maxInt_Success_Test() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertEquals(0, myBranching.maxInt(1, -1));
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(Mockito.anyString());
    }
    /**
     * Проверяем неуспешный сценарий выполнения maxInt
     */
    @Test
    public void maxInt_Negative_Test() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.maxInt(-1, 1);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }
    /**
     * Проверяем успешный сценарий выполнения utilFunk2()
     */
    @Test
    public void ifElseExample_Success_Test() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        boolean result = myBranching.ifElseExample();

        Assertions.assertTrue(result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }
    /**
     * Проверяем неуспешный сценарий выполнения utilFunc2()
     */
    @Test
    public void ifElseExample_Unsuccessful_Test() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        boolean result = myBranching.ifElseExample();

        Assertions.assertFalse(result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }
    /**
     * Проверяем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
     */
    @Test
    public void switchExample_input_0_Test() {
        final int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(i)).utilFunc1(anyString());
    }
    /**
     * Проверяем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 1
     */
    @Test
    public void switchExample_input_1_Test() {
        final int i = 1;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(i)).utilFunc1(anyString());
    }
    /**
     * Проверяем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 2
     */
    @Test
    public void switchExample_input_2_Test() {
        final int i = 2;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }
}
